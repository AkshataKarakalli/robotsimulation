class RobotsController < ApplicationController
    def orders
        commands = params[:commands]
        result = Robot.execute(commands)
        render json: { location: result }, status: :ok
    end
end
