class Robot 
  class << self
   # directions
  NORTH = 'NORTH'
  SOUTH = 'SOUTH'
  EAST = 'EAST'
  WEST = 'WEST'

  # commands
  PLACE = 'PLACE'
  MOVE = 'MOVE'
  LEFT = 'LEFT'
  RIGHT = 'RIGHT'

  REPORT = 'REPORT'

  DIRECTION_MAPPER = { "NORTH_LEFT": 'WEST', "NORTH_RIGHT": 'EAST',
                       "SOUTH_LEFT": 'EAST', "SOUTH_RIGHT": 'WEST',
                       "EAST_LEFT": 'NORTH', "EAST_RIGHT": 'SOUTH',
                       "WEST_LEFT": 'SOUTH', "WEST_RIGHT": 'NORTH' }.freeze
attr_accessor :x, :y, :orientation
  
  def initialize
    @x = 0
    @y = 0
    @orientation = NORTH
  end

  def execute(commands)
    result = []
    commands.each do |command|
      if command[0..4] == PLACE
        place_robot(command)
      elsif command == MOVE
        move_robot
      elsif [LEFT, RIGHT].include?(command)
        @orientation = DIRECTION_MAPPER["#{@orientation}_#{command}".to_sym] # Rotate robot 90 degree
      elsif command == REPORT
        result.push(@x, @y, @orientation)
      end
    end
    result
  end

  def move_robot
    return unless valid_move?
    if @orientation == NORTH
      @y += 1
    elsif @orientation == SOUTH
      @y -= 1
    elsif @orientation == EAST
      @x += 1
    elsif @orientation == WEST
      @x -= 1
    end
  end

  def valid_move?
    (0...5) === @x && (0...5) === @y
  end

  def place_robot(command)
    cords = command.split(' ')[1].split(',')
    @x = cords[0].to_i
    @y = cords[1].to_i
    @orientation = cords[2]

  end
 end
end
