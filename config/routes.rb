Rails.application.routes.draw do
  scope :api do
    post 'robot/0/orders', action: :orders, controller: :robots
  end
end
